set(CMAKE_INSTALL_RPATH "${PLUGIN_MODULE_DIR}")

set(CMAKE_AUTOMOC ON)
set(QML_SOURCES
    ClickUpdateDelegate.qml
    Configuration.qml
    ChangelogExpander.qml
    DownloadHandler.qml
    GlobalUpdateControls.qml
    ImageUpdatePrompt.qml
    PageComponent.qml
    UpdateDelegate.qml
    UpdateSettings.qml
    ChannelSettings.qml
    ReinstallAllApps.qml
    FirmwareUpdate.qml
    i18nd.js
    ChUtils.js
)

include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    /usr/include/apt-pkg/
)

add_library(UpdatePlugin SHARED
    click/apiclient.h
    click/manager.h
    click/manifest.h

    click/apiclient_impl.cpp
    click/manifest_impl.cpp
    click/manager_impl.cpp

    image/imagemanager.h
    image/imagemanager_impl.cpp

    network/accessmanager.h
    network/accessmanager_impl.cpp

    helpers.cpp
    update.cpp
    updatedb.cpp
    updatemodel.cpp
    updatemanager.cpp
)
target_link_libraries(UpdatePlugin
    Qt5::Core
    Qt5::DBus
    Qt5::Gui
    Qt5::Network
    Qt5::Quick
    Qt5::Sql

    PkgConfig::AptPkg
    PkgConfig::LomiriSystemSettings
    LomiriSystemSettingsPrivate
)
target_compile_definitions(UpdatePlugin
    PUBLIC
    PLUGIN_PRIVATE_MODULE_DIR="${PLUGIN_PRIVATE_MODULE_DIR}"
)

add_library(LomiriUpdatePanel MODULE
    plugin.cpp
    ${QML_SOURCES}
)
target_link_libraries(LomiriUpdatePanel Qt5::Qml Qt5::Quick Qt5::DBus UpdatePlugin)

set(PLUG_DIR ${PLUGIN_PRIVATE_MODULE_DIR}/Lomiri/SystemSettings/Update)
install(TARGETS UpdatePlugin DESTINATION ${PLUGIN_MODULE_DIR})
install(TARGETS LomiriUpdatePanel DESTINATION ${PLUG_DIR})
install(FILES qmldir.in DESTINATION ${PLUG_DIR} RENAME qmldir)
install(FILES ${QML_SOURCES} DESTINATION ${PLUGIN_QML_DIR}/system-update)

install(FILES system-update.settings DESTINATION ${PLUGIN_MANIFEST_DIR})
install(FILES settings-system-update.svg DESTINATION ${PLUGIN_MANIFEST_DIR}/icons)

set(QML_SOURCES_NOTIFICATION EntryComponent.qml)

# We need a dummy target so the QML files show up in Qt Creator
# If this plugin gets some C++ sources, remove this.
add_custom_target(update-notification
COMMAND echo This is just a dummy.
SOURCES ${QML_SOURCES_NOTIFICATION})

install(FILES update-notification.settings DESTINATION ${PLUGIN_MANIFEST_DIR})
install(FILES ${QML_SOURCES_NOTIFICATION} DESTINATION ${PLUGIN_QML_DIR}/update-notification)
